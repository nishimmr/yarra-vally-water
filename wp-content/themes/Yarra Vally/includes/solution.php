
<div class="container solution"> 

<div class="col-sm-12">
    <h1 class="solution-title"> An End-to-End Business Solution</h1>
<?php $bronze_spon = get_field('banners','option');
 if($bronze_spon) {
foreach($bronze_spon as $banner) {
?>
  
    <div class="col-sm-4 col-xs-12 busness-sol"> <img src="<?php echo $banner['image'];?>"> </div>

<?php }
}
?>
</div>


<div class="col-sm-12 col-xs-12 bottom-part">
<div class="col-sm-6 col-xs-12">
<h2> Upstream </h2>
<p> The Upper Yarra Mineral Springs Company’s upstream integrated supply chain allows the company to secure and manage the supply, bottling and delivery of premium and competitively priced Australian bottled water to international markets. The Upper Yarra Mineral Springs Company will market its bottled water utilising provenance and point-of-origin branding, traceability and the clean and green image of Australian produce. </p>
  </div>

<div class="col-sm-6 col-xs-12">
<h2> Downstream </h2>
<p> The Upper Yarra Mineral Springs Company’s downstream supply chain will allow distributors in international markets
(e.g. UAE and China) to leverage the Upper Yarra Mineral Springs Company’s premium Australian water quality and branding to promote and drive sales of the Upper Yarra Mineral Springs Company’s water to existing networks.
This will also further promote and market the water in international markets. </p>
  </div>

</div>
</div>