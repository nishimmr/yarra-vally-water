	<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}
?>
	<!doctype html>
	<!--[if !IE]>
	<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 7 ]>
	<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 8 ]>
	<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 9 ]>
	<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
	<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta charset="<?php bloginfo( 'charset' ); ?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<title><?php wp_title( '&#124;', true, 'right' ); ?></title>

		<?php wp_head(); ?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
		
	
	</head>

<body <?php body_class(); ?>>

<?php $backheader =  get_field('header_background','option'); ?>
 


<div class="top-header" style="background:url('<?php echo $backheader;?>');background-repeat: no-repeat;background-size: cover;"> 
	

	<div class="container">
	<div class="col-md-5 col-sm-7 col-xs-10 header-logo">
		<div class="top-logo"> <a href="<?php echo home_url( ); ?>" > <img src="<?php echo get_field('top_logo','option');?>"></a></div>
		 
	</div>

	<div class="col-md-3 col-sm-5">
	<?php // get_template_part( 'loop-header' ); ?>
	</div>

			
	 
			</div>
		
	 </div>
</div>
	
</div>

 
<!-- Nav Wrap END -->

