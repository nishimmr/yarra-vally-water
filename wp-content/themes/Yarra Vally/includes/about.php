<div class="container about">

  
    <div class="col-sm-5 col-xs-12 left">
     <h1>A unique opportunity to stake a claim in the lucrative global spring water industry leveraging a clean, green, protected Australian brand. </h1>

    
      <button id="contact-us"> Contact Us</button>
   
     </div>
     <div class="col-sm-7 col-xs-12 right">
<p>The Upper Yarra Mineral Springs Company is a water brokerage business with an integrated supply chain and has a vision of bottling a premium brand of spring water from its Victorian sources (the southern slopes of the Great Dividing Range) with a range of chemistry to satisfy all palates to meet international demand.   </p>

<p>There is an opportunity to stake a claim in the lucrative global spring water industry, leveraging a clean, green, protected Australian brand. </p>

<p>Uniquely, we can offer a total turn-key branded solution for an overseas distributor. While the total sales of bottled water in Australia is approximately 1,200 megalitres, the real demand is in international markets such as Central America, Asia and the Middle East. With the intention, research and capability to crack into these profitable markets, the bar has been set high for success.</p>

<p>While overseas markets are firmly in our sights, we are at the core a local product. Our roots, beliefs and inspiration derive from the Yarra Valley. When we combine what we have been gifted naturally from the iconic Yarra Valley with what we have worked so hard to develop and create, we are in an unprecedented position for growth and development.</p>

<p>Premium bottled water walks the line of commodity and luxury – an idea that we are passionate about. We invite all prospective investors to share in our excitement.</p>

      </div>
  
</div>